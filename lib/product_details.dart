import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';




class ProductDetails extends StatefulWidget {
  final String _image;
  final String _title;
  final String _description;
  final int _price;
  final int _quantity;
  final String _link;

  const ProductDetails(
      this._image, this._title, this._description, this._price, this._quantity, this._link,{Key? key}) : super(key: key);

  @override
  State<ProductDetails> createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  late int _currentQuantity;

  @override
  void initState() {
    _currentQuantity = widget._quantity;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._title),
      ),
      body: Column(
        children: [
          Container(
            width: double.infinity,
            margin: const EdgeInsets.fromLTRB(20, 0, 20, 20),
            child: Image.asset(widget._image, width: 460, height: 215)
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(20, 0, 20, 50),
            child: Text(widget._description),
          ),
          Text(widget._price.toString() + " TND", textScaleFactor: 3),
          Text("Exemplaires disponibles : " + _currentQuantity.toString()),
          const SizedBox(
            height: 5,
          ),
          QrImage(
            data: widget._link,
            version: QrVersions.auto,
            size: 120.0,
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment:
          MainAxisAlignment.spaceEvenly,

            children: [
              SizedBox(
                width: 150,
                height: 50,
                child: ElevatedButton.icon(
                    label: const Text("Acheter", textScaleFactor: 1),
                    icon: const Icon(Icons.shopping_basket_rounded),
                    onPressed: () {
                      setState(() {
                        _currentQuantity--;
                      });
                    }
                ),
              ),
              SizedBox(
                width: 150,
                height: 50,
                child: ElevatedButton.icon(
                    label: const Text("Map", textScaleFactor: 1),
                    icon: const Icon(Icons.shopping_basket_rounded),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, "/Favorite");
                    }
                ),
              )
            ],


          )


        ],
      ),
    );
  }
}