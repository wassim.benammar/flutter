
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:workshop_gamix2122/home/product_info.dart';

class SQLService {
  Database? db;

  Future openDB() async {
    try {
      // Get a location using getDatabasesPath
      var databasesPath = await getDatabasesPath();
      String path = join(databasesPath, 'shopping.db');

      // open the database
      db = await openDatabase(
        path,
        version: 1,
        onCreate: (Database db, int version) async {
          print(db);
          this.db = db;
          createTables();
        },
      );
      return true;
    } catch (e) {
      print("ERROR IN OPEN DATABASE $e");
      return Future.error(e);
    }
  }

  createTables() async {
    try {
      var qry = "CREATE TABLE IF NOT EXISTS product ( "

          "image Text,"
          "title TEXT PRIMARY KEY,"
          "description TEXT,"
          "price INTEGER,"
          "quantity INTEGER)";
      await db?.execute(qry);
      qry = "CREATE TABLE IF NOT EXISTS cart_list ( "

          "image Text,"
          "title TEXT PRIMARY KEY,"
          "description TEXT,"
          "price INTEGER,"
          "quantity INTEGER)";

      await db?.execute(qry);
    } catch (e) {
      print("ERROR IN CREATE TABLE");
      print(e);
    }
  }

  Future saveRecord(Product data) async {
    await this.db?.transaction((txn) async {
      var qry =
          'INSERT INTO cart_list(image, title, description,price,quantity) VALUES("${data.image}",${data.title}, "${data.description}",${data.price},${data.quantity})';
      int id1 = await txn.rawInsert(qry);
      return id1;
    });
  }

  Future setItemAsFavourite(int id, bool flag) async {
    var query = "UPDATE shopping set fav = ? WHERE id = ?";
    return await this.db?.rawUpdate(query, [flag ? 1 : 0, id]);
  }

  Future getItemsRecord() async {
    try {
      var list = await db?.rawQuery('SELECT * FROM product', []);
      return list ?? [];
    } catch (e) {
      return Future.error(e);
    }
  }

  Future getCartList() async {
    try {
      var list = await db?.rawQuery('SELECT * FROM cart_list', []);
      return list ?? [];
    } catch (e) {
      return Future.error(e);
    }
  }

  Future addToCart(Product data) async {
    await this.db?.transaction((txn) async {
      var qry =
          'INSERT INTO cart_list(image, title, description,price,quantity) VALUES("${data.image}",${data.title}, "${data.description}",${data.price},${data.quantity})';
      int id1 = await txn.rawInsert(qry);
      return id1;
    });
  }

  Future removeFromCart(String tt) async {
    var qry = "DELETE FROM cart_list where id = ${tt}";
    return await this.db?.rawDelete(qry);
  }
}
